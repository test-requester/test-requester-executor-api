package com.gitlab.testrequester;

import org.assertj.core.api.AbstractByteArrayAssert;
import org.assertj.core.api.AbstractIntegerAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class RequestExecutorAbstractTest {

    private final EchoService echoService = new EchoService();
    private RequestExecutor executor;

    protected abstract RequestExecutor setupExecutor(EchoService echoService);

    @BeforeEach
    void beforeEachRequesterTest() {
        this.executor = setupExecutor(echoService);
    }

    @Test
    void test_headMethod() {
        // ARRANGE
        // ACT
        ResponseData response = executor.execute(newRequest("HEAD", "/echo").build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEmpty(); // HEAD cannot receive a body as response

        assertThat(echoService.getMethod()).as("Method").isEqualTo("HEAD");
        assertThat(echoService.getUri()).as("URI").isEqualTo("/echo");
        assertThat(echoService.getBody()).as("Body not given").isNull();
        assertThat(echoService.getHeaders()).as("Headers").isEmpty();
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    public RequestData.Builder newRequest(String method, String uri) {
        return RequestData.builder().method(method).uri(URI.create(uri));
    }

    private AbstractIntegerAssert<?> assertStatus(ResponseData response) {
        return assertThat(response.getStatus()).as("Response Status");
    }

    private AbstractByteArrayAssert<?> assertBody(ResponseData response) {
        return assertThat(response.getBody()).as("Response Body");
    }

    @Test
    void test_optionsMethod() {
        // ACT
        ResponseData response = executor.execute(newRequest("OPTIONS", "/echo").build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEmpty(); // OPTIONS cannot receive a body as response

        // Endpoint logic not executed for OPTIONS, hence sink is all empty
        assertThat(echoService.getMethod()).as("Method").isNull();
        assertThat(echoService.getUri()).as("URI").isNull();
        assertThat(echoService.getBody()).as("Body not given").isNull();
        assertThat(echoService.getHeaders()).as("Headers").isEmpty();
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_getMethod() {
        // ACT
        ResponseData response = executor.execute(newRequest("GET", "/echo").build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEmpty();

        assertThat(echoService.getMethod()).as("Method").isEqualTo("GET");
        assertThat(echoService.getUri()).as("URI").isEqualTo("/echo");
        assertThat(echoService.getBody()).as("Body not given").isNull();
        assertThat(echoService.getHeaders()).as("Headers").isEmpty();
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_deleteMethod() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("DELETE", "/echo")
                        .body("delete-body".getBytes(StandardCharsets.ISO_8859_1))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEqualTo("delete-body".getBytes(StandardCharsets.ISO_8859_1));

        assertThat(echoService.getMethod()).as("Method").isEqualTo("DELETE");
        assertThat(echoService.getUri()).as("URI").isEqualTo("/echo");
        assertThat(echoService.getBody()).as("Body").isEqualTo("delete-body");
        assertThat(echoService.getHeaders()).containsEntry("Content-Length", listOf("11"));
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_postMethod() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("POST", "/echo")
                        .body("post-body".getBytes(StandardCharsets.ISO_8859_1))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEqualTo("post-body".getBytes(StandardCharsets.ISO_8859_1));

        assertThat(echoService.getMethod()).as("Method").isEqualTo("POST");
        assertThat(echoService.getUri()).as("URI").isEqualTo("/echo");
        assertThat(echoService.getBody()).as("Body").isEqualTo("post-body");
        assertThat(echoService.getHeaders()).containsEntry("Content-Length", listOf("9"));
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_putMethod() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("PUT", "/echo")
                        .body("put-body".getBytes(StandardCharsets.ISO_8859_1))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEqualTo("put-body".getBytes(StandardCharsets.ISO_8859_1));

        assertThat(echoService.getMethod()).as("Method").isEqualTo("PUT");
        assertThat(echoService.getUri()).as("URI").isEqualTo("/echo");
        assertThat(echoService.getBody()).as("Body").isEqualTo("put-body");
        assertThat(echoService.getHeaders()).containsEntry("Content-Length", listOf("8"));
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_patchMethod() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("PATCH", "/echo")
                        .body("patch-body".getBytes(StandardCharsets.ISO_8859_1))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertBody(response).isEqualTo("patch-body".getBytes(StandardCharsets.ISO_8859_1));

        assertThat(echoService.getMethod()).as("Method").isEqualTo("PATCH");
        assertThat(echoService.getUri()).as("URI").isEqualTo("/echo");
        assertThat(echoService.getBody()).as("Body").isEqualTo("patch-body");
        assertThat(echoService.getHeaders()).containsEntry("Content-Length", listOf("10"));
        assertThat(echoService.getParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_queryParams() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("GET", "/echo")
                        .queryParams(mapOf(params -> {
                            params.put("key", listOf("{url encode me}"));
                            params.put("numbers", listOf("one", "two", "three"));
                        }))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertThat(echoService.getParams())
                .containsEntry("key", listOf("{url encode me}"))
                .containsEntry("numbers", listOf("one", "two", "three"));
    }

    @Test
    void test_requestParams() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("GET", "/echo")
                        .requestParams(mapOf(params -> {
                            params.put("key", listOf("{url encode me}"));
                            params.put("numbers", listOf("one", "two", "three"));
                        }))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertThat(echoService.getParams())
                .containsEntry("key", listOf("{url encode me}"))
                .containsEntry("numbers", listOf("one", "two", "three"));
    }

    @Test
    void test_headers() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("GET", "/echo")
                        .headers(mapOf(headers -> {
                            headers.put("Authorization", listOf("Bearer j.w.t"));
                            headers.put("Origin", listOf("localhost"));
                        }))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);
        assertThat(echoService.getHeaders())
                .containsEntry("Authorization", listOf("Bearer j.w.t"))
                .containsEntry("Origin", listOf("localhost"));
    }

    @Test
    void test_everythingAtOnce() {
        // ACT
        ResponseData response = executor
                .execute(newRequest("POST", "/echo")
                        .queryParams(mapOf(params -> params.put("foo", listOf("bar", "{escape-me}"))))
                        .requestParams(mapOf(params -> params.put("baz", listOf("quux"))))
                        .headers(mapOf(headers -> headers.put("Authorization", listOf("Bearer j.w.t"))))
                        .body("body was here".getBytes(StandardCharsets.ISO_8859_1))
                        .build());

        // ASSERT
        assertStatus(response).isEqualTo(200);

        assertThat(echoService.getMethod()).as("HTTP method")
                .isEqualTo("POST");
        assertThat(echoService.getUri()).as("Path variable id")
                .isEqualTo("/echo");
        assertThat(echoService.getBody()).as("RequestBody")
                .isEqualTo("body was here");
        assertThat(echoService.getHeaders()).as("Headers")
                .containsEntry("Authorization", listOf("Bearer j.w.t"));
        assertThat(echoService.getParams()).as("Query params")
                .containsEntry("foo", listOf("bar", "{escape-me}"))
                .containsEntry("baz", listOf("quux"));
    }

    @SafeVarargs
    private final <E> List<E> listOf(E... elements) {
        return Collections.unmodifiableList(new ArrayList<>(Arrays.asList(elements)));
    }

    private <K, V> Map<K, V> mapOf(Consumer<Map<K, V>> builder) {
        Map<K, V> map = new HashMap<>();
        builder.accept(map);
        return Collections.unmodifiableMap(map);
    }
}
