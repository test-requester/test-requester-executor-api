package com.gitlab.testrequester;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EchoService {
    String method;

    String uri;

    String body;

    @NonNull
    Map<String, List<String>> headers = Collections.emptyMap();

    @NonNull
    Map<String, List<String>> params = Collections.emptyMap();

    public String callMe(
            String method,
            String uri,
            String body,
            Map<String, List<String>> headers,
            Map<String, List<String>> params
    ) {
        this.method = method;
        this.uri = uri;
        this.body = body;
        this.headers = headers;
        this.params = params;
        return body;
    }
}
