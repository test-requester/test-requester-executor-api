package com.gitlab.testrequester;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.NonNull;
import lombok.Value;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Value
@Builder(builderClassName = "Builder")
public class RequestData {
    @NonNull String method;

    @NonNull URI uri;

    @Default
    @NonNull Map<String, List<String>> queryParams = Collections.emptyMap();

    @Default
    @NonNull Map<String, List<String>> requestParams = Collections.emptyMap();

    @Default
    @NonNull Map<String, List<String>> headers = Collections.emptyMap();

    @Default
    @NonNull byte[] body = new byte[0];
}
