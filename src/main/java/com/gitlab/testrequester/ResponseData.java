package com.gitlab.testrequester;

import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
public class ResponseData {
    Object raw;
    int status;
    Map<String, List<String>> headers;
    byte[] body;
}
