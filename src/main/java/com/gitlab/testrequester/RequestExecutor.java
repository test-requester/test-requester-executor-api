package com.gitlab.testrequester;

import lombok.NonNull;

/**
 * A plugin for {@code com.gitlab.testrequester.TestRequester} to execute a request.
 * <p>
 * Implementations can execute requests via spring's mock mvc or via real http clients.
 */
public interface RequestExecutor {
    @NonNull
    ResponseData execute(@NonNull RequestData request);
}
